﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BusSet : MonoBehaviour
{
    public GameObject bus;
    public Vector3 position;
    public Vector3 angle;

    private void OnEnable()
    {
        bus.transform.position = position;
        bus.transform.rotation = Quaternion.Euler(angle.x, angle.y, angle.z);
    }
}
