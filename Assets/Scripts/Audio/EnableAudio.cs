﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableAudio : MonoBehaviour
{
    public AudioClip womanAudioClip;
    public AudioClip manAudioClip;
    private AudioSource audioSource;
    
    private void OnEnable()
    {
        audioSource = FindObjectOfType<AudioSource>();
        StartCoroutine(WaitToPlay());
    }
    
    private IEnumerator WaitToPlay(){
        yield return new WaitWhile (()=> audioSource.isPlaying);
        audioSource.clip = PlayerPrefs.GetString("sex") == "man" ? manAudioClip : womanAudioClip;
        audioSource.Play();
    }
}
