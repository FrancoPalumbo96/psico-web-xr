﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class RemoveEventTrigger : MonoBehaviour
{
    public EventTrigger eventTrigger;
    void Start()
    {
        eventTrigger.triggers.RemoveRange (0, eventTrigger.triggers.Count);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
