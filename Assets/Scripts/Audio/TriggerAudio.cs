﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

public class TriggerAudio : MonoBehaviour
{
    public AudioClip womanAudioClip;
    public AudioClip manAudioClip;
    private AudioSource audioSource;
    
    private EventTrigger eventTrigger;

    private bool wasActivated;
    public event Action onAudioFinished;
    public void Start()
    {
        audioSource = FindObjectOfType<AudioSource>();
    }

    public void ActivateAudio()
    {
        if(wasActivated)
            return;
        if(!this.isActiveAndEnabled)
            return;
        
        wasActivated = true;
        audioSource.clip = PlayerPrefs.GetString("sex") == "man" ? manAudioClip : womanAudioClip;
        audioSource.Play();
        StartCoroutine(playSound());
    }
    
    
    IEnumerator playSound()
    {
        float time = PlayerPrefs.GetString("sex") == "man" ? manAudioClip.length : womanAudioClip.length;
        yield return new WaitForSeconds(time);
        AudioFinished();
    }
    private void AudioFinished()
    {
        if (onAudioFinished != null)
        {
            onAudioFinished(); 
        }
    }
}
