﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerAnimation : MonoBehaviour
{
    public Animator animator;
    private static readonly int MoveHeadLeft = Animator.StringToHash("MoveHeadLeft");

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            animator.SetTrigger(MoveHeadLeft);
        } 
    }
}
