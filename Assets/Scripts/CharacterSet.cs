﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSet : MonoBehaviour
{
    public GameObject womanModel;
    public GameObject manModel;
    private void OnEnable()
    {
        var sex = PlayerPrefs.GetString("sex");
        setModels(sex != "man");
    }

    private void setModels(bool activateMan)
    {
        womanModel.SetActive(!activateMan);
        manModel.SetActive(activateMan);
    }
}
