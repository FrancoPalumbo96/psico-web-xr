﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CSCAll : ChangeSceneConditions
{
    public override bool ConditionsMet()
    {
        return conditions.All(cond => cond.isConditionMet());
    }
}
