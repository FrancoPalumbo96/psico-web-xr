﻿using System;
using UnityEngine;

public class Condition: MonoBehaviour
{
    private bool conditionState = false;

    public event Action onTriggerCondition;

    private TriggerAudio audio;

    private void Start()
    {
        audio = gameObject.GetComponent<TriggerAudio>();
        audio.onAudioFinished += setCondition;
    }

    public bool isConditionMet()
    {
        return conditionState;
    }

    public void acceptCondition()
    {
        TriggerCondition();
    }

    public void setCondition()
    {
        conditionState = true;

    }
    
    
    private void TriggerCondition()
    {
        if (onTriggerCondition != null)
        {
            onTriggerCondition(); 
        }
    }
    
    
    
}
