﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CSCOnlyOne : ChangeSceneConditions
{
    public override bool ConditionsMet()
    {
        return conditions.Any(cond => cond.isConditionMet());
    }
}
