﻿
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.EventSystems;

public abstract class ChangeSceneConditions : MonoBehaviour
{
    public List<Condition> conditions;

    public abstract bool ConditionsMet();

    private void Start()
    {
        foreach (Condition cond in conditions)
        {
            /*InteractableObjectEventHandler pimba = (obj, gameObject) => SirenActivated();
            sirenVRTK.InteractableObjectTouched += pimba;*/

            
            
            cond.onTriggerCondition += () => OnTriggerCondition(cond);
            cond.gameObject.GetComponent<TriggerAudio>().onAudioFinished += OnAudioFinished;
        }
    }

    private void OnAudioFinished()
    {
        //Unblock
        foreach (Condition cond in conditions)
        {
            cond.GetComponent<EventTrigger>().enabled = true;
        }
    }

    private void OnTriggerCondition(Condition condition)
    {
        //Block all
        foreach (Condition cond in conditions)
        {
            if (cond != condition)
                cond.GetComponent<EventTrigger>().enabled = false;
            /*else
            {
                condition.GetComponent<TriggerAudio>().enabled = false;
            }*/
            
        }
        
     
    }

    private void DeleteEventTriggers(Condition condition)
    {
        /*EventTrigger trigger = condition.gameObject.GetComponent<EventTrigger>();
     List<EventTrigger.Entry> entriesToRemove = new List<EventTrigger.Entry>();

//finding required entry by eventId
     foreach (var entry in trigger.triggers)
     {        
         if (entry.eventID == EventTriggerType.PointerEnter)
         {
             //remove listener from entry
             entry.callback.RemoveAllListeners();
             //add entry to transitional list
             entriesToRemove.Add(entry);
         }
     }

//remove all entries satisfied condition from triggerlist
     foreach(var entry in entriesToRemove)
     {
         trigger.triggers.Remove(entry);
     }*/
    }
}
