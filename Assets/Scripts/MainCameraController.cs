﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCameraController : MonoBehaviour
{

    private Camera camera;
    private bool calledEvent;

    private void Start()
    {
        camera = GetComponent<Camera>();
        calledEvent = false;
    }

    // Update is called once per frame
    private void Update()
    {
        if (!camera.isActiveAndEnabled && !calledEvent)
        {
            calledEvent = true;
            GameEvents.current.CameraChange();
        }

        if (camera.isActiveAndEnabled && calledEvent)
        {
            calledEvent = false;
            GameEvents.current.CameraChange();
        }
    }
}
