﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyCameraDrag : MonoBehaviour
{
    private float xRotation = 0.0f;
    private float yRotation = 0.0f;
    private Camera cam;
    
    private Vector3 dragOrigin;
    public float dragSpeed = 0.5f;
    
    void Start()
    {
        cam = GetComponent<Camera>();
    }
 
    void Update()
    {
        
        if (Input.GetMouseButtonDown(0))
        {
            dragOrigin = Input.mousePosition;
            return;
        }
        
        if (!Input.GetMouseButton(0)) return;
        
        Vector3 pos = cam.ScreenToViewportPoint(Input.mousePosition - dragOrigin);
        Vector3 move = new Vector3(pos.x * dragSpeed, 0, pos.y * dragSpeed);
        
 
        yRotation += move.x;
        xRotation -= move.z;
        xRotation = Mathf.Clamp(xRotation, -90, 90);

        cam.transform.eulerAngles = new Vector3(xRotation, yRotation, 0.0f);
    }

    public void ResetXYRotation()
    {
        Debug.Log("Reseted");
        xRotation = 0.0f;
        yRotation = 0.0f;
        dragOrigin = new Vector3(0,0,0);
    }
}
