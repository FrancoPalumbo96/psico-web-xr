﻿using System;
using UnityEngine;

public class GameEvents : MonoBehaviour
{
    public static GameEvents current;
    // Start is called before the first frame update
    private void Awake()
    {
        current = this;
        DontDestroyOnLoad(this);
    }

    public event Action onCameraChange;
    public void CameraChange()
    {
        if (onCameraChange != null)
        {
            onCameraChange(); 
        }
    }
    
}
