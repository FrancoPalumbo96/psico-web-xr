﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    // Start is called before the first frame update
    public GvrReticlePointer pointer;
    public Camera mainCamera;
    public Camera eyeCamera;

    public bool isPointerInMain;
    
    [DllImport("__Internal")]
    private static extern bool IsMobile();

    private MyCameraDrag cameraDrag;
    
    void Start()
    {
        GameEvents.current.onCameraChange += OnCameraChange;
        isPointerInMain = true;
        cameraDrag = GetComponent<MyCameraDrag>();
    }

    private void OnCameraChange()
    {
        if (isPointerInMain)
        {
            var transform1 = pointer.transform;
            transform1.parent = eyeCamera.transform;
            pointer.overridePointerCamera = eyeCamera;
            transform1.localPosition = new Vector3(0,0,0);
            transform1.rotation = Quaternion.identity;
            isPointerInMain = false;
            cameraDrag.enabled = false;
        }
        else
        {
            cameraDrag.enabled = isMobile();

            var transform1 = pointer.transform;
            transform1.parent = mainCamera.transform;
            pointer.overridePointerCamera = mainCamera;
            transform1.localPosition = new Vector3(0, 0, 0);
            transform1.rotation = Quaternion.identity;
            isPointerInMain = true;
        }
    }


    private bool isMobile()
    {
#if !UNITY_EDITOR && UNITY_WEBGL
             return IsMobile();
#endif
        return false;
    }
}
