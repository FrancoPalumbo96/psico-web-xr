﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour
{
    public int sceneNumber = 1;
    public void Change()
    {
        SceneManager.LoadScene(sceneNumber, LoadSceneMode.Single);
    }
}
