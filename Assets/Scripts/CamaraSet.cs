﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamaraSet : MonoBehaviour
{
    private GameObject WebXRCameraSet;
    public Vector3 position;
    public Vector3 angle;

    private MyCameraDrag cameraDrag;
    
    private void OnEnable()
    {
        WebXRCameraSet = GameObject.Find("WebXRCameraSet");

        cameraDrag = FindObjectOfType<MyCameraDrag>();
        cameraDrag.ResetXYRotation();
        
        //camera.transform.eulerAngles = new Vector3(0,0,0);
        
        WebXRCameraSet.transform.position = position;
        WebXRCameraSet.transform.rotation = Quaternion.Euler(angle.x, angle.y, angle.z);
    }
    
    private float AdjustOnChangeScene(float actualYAngle, float setYAngle)
    {
        return Math.Abs(actualYAngle - setYAngle);
    }
    
    

    

    // Update is called once per frame
   
}
