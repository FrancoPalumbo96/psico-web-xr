﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public abstract class TimeActivation : MonoBehaviour
{
    protected bool isActive = false;
    public Coroutine activateByTime;


    public Action callback;
    public Action done;
    

    protected void Callback()
    {
        isActive = false;
        if(activateByTime != null)
            StopCoroutine(activateByTime);
        callback?.Invoke();
    }

    protected void ActivateByTime(float time)
    {
        isActive = true;
//        Debug.Log("Started Coroutine");
        activateByTime = StartCoroutine(WaitTimeToActivate(time, done));
    }

    private IEnumerator WaitTimeToActivate(float time, Action action)
    {
        float passedTime = time;
//        Debug.Log("In Coroutine -> time & action: " + time + " / " + action);
//        Debug.Log("Is Active" + isActive);
        if (!isActive)
        {
            yield return null;
        }
        yield return new WaitForSeconds(passedTime);
//        Debug.Log("Finished Coroutine -> action()");
        action();
    }
    
}
