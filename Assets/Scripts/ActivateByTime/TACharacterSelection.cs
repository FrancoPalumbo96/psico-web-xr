﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TACharacterSelection : TimeActivation
{
    public bool isMan;
    public TAActivateScene activateScene;
    void Start()
    {
        done = CharacterSelected;
        callback = null;
        activateScene = GetComponent<TAActivateScene>();
    }

    public void PointerEnter()
    {
        ActivateByTime(1f);
    }

    public void PointerExit()
    {
        Callback();
    }
    
    private void CharacterSelected()
    {
        PlayerPrefs.SetString("sex", isMan ? "man" : "woman");
        activateScene.CallActivate();
    }
}
