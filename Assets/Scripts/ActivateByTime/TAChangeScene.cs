﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TAChangeScene : TimeActivation
{
    public int sceneNumber = 1;
    void Start()
    {
        done = Change;
        callback = null;
    }

    public void PointerEnter()
    {
        ActivateByTime(1);
    }

    public void PointerExit()
    {
        Callback();
    }

    private void Change()
    {
        SceneManager.LoadScene(sceneNumber, LoadSceneMode.Single);
    }
}
