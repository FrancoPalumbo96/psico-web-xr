﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TAActivateScene : TimeActivation
{
    public GameObject[] activate;
    public GameObject[] deactivate;

    public ChangeSceneConditions conditions;
    public float activationTime = 1f;
    
    
    void Start()
    {
        done = Activate;
        callback = null;
    }

    public void PointerEnter()
    {

        if ((conditions == null || conditions.ConditionsMet()) && !isActive)
        {
            ActivateByTime(activationTime);
        }
    }

    public void PointerExit()
    {
        //Callback();
    }


    public void CallActivate()
    {
        Activate();
    }

    private void Activate()
    {
        if (activate.Length > 0)
        {
            foreach (GameObject go in activate)
            {
                go.SetActive(true);
            }
        }

        if (deactivate.Length > 0)
        {
            foreach (GameObject go in deactivate)
            {
                go.SetActive(false);
            }
        }
    }
}
