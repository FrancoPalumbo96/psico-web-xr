﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TAOnEnableChange : MonoBehaviour
{
    private TAActivateScene activateScene;
    private void Awake()
    {
        activateScene = GetComponent<TAActivateScene>();
    }

    private void OnEnable()
    {
        StartCoroutine(Wait(8, activateScene.CallActivate));
    }

    private IEnumerator Wait(float time, Action action)
    {
        yield return new WaitForSeconds(time);
        action();
    }
}
