﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TARotate : TimeActivation
{

    public Transform part;
    public float rotationTime;
    public bool rotateLeft;
    void Start()
    {
        done = Rotate;
        callback = null;
        
    }

    
    public void PointerEnter()
    {
        ActivateByTime(1);
    }

    public void PointerExit()
    {
        Callback();
    }

    private void Rotate()
    {
        //BUG Con el nombre Head no funciona ???
        //TODO add coroutine
        
        StartCoroutine(RotateSlowly());
    }

    IEnumerator RotateSlowly()
    {
        float startRotation = part.eulerAngles.y;
        float endRotation;
        if (rotateLeft)
        {
            endRotation = startRotation + 90.0f;
        }
        else
        {
            endRotation = startRotation - 90.0f;
        }
        float t = 0.0f;
        while (t < rotationTime)
        {
            t += Time.deltaTime;
            float yRotation = Mathf.Lerp(startRotation, endRotation, t / rotationTime) % 360.0f;
            var eulerAngles = part.eulerAngles;
            eulerAngles = new Vector3(eulerAngles.x, yRotation,
                eulerAngles.z);
            part.eulerAngles = eulerAngles;
            yield return null;
        }
    }
}
