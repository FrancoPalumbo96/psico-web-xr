﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TAChangeColor : TimeActivation
{
    public Color color = Color.blue;
    void Start()
    {
        done = NewColor;
        callback = DisableMeshRenderer;
    }

    public void PointerEnter()
    {
        ActivateByTime(0.5f);
    }

    public void PointerExit()
    {
        Callback();
    }
    
    private void NewColor()
    {
        Renderer renderer = GetComponent<Renderer>();
        renderer.material.color = color;
        renderer.enabled = true;
    }

    //TODO change to original state 
    private void DisableMeshRenderer()
    {
        GetComponent<Renderer>().enabled = false;
    }
    
}
