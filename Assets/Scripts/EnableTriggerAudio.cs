﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableTriggerAudio : MonoBehaviour
{
    private TriggerAudio senderTriggerAudio;
    public TriggerAudio receiverTriggerAudio;

    private void Start()
    {
        senderTriggerAudio = gameObject.GetComponent<TriggerAudio>();
        senderTriggerAudio.onAudioFinished += EnableAudio;
    }

    private void EnableAudio()
    {
        receiverTriggerAudio.enabled = true;
    }
}
