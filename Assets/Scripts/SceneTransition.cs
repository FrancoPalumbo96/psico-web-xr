﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SceneTransition : MonoBehaviour
{

    private float time;
    private int timeInSeconds;
    private bool started;

    public int timePeopleLeaving = 5;
    public List<GameObject> people;

    public TAActivateScene taActivateScene;
    // Start is called before the first frame update
    void Start()
    {
        time = 0;
        timeInSeconds = 0;
        started = true;
    }

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;
        timeInSeconds = (int) Mathf.Floor(time);
        
        if (!started) return;
        if (timeInSeconds != timePeopleLeaving) return;
        GetOffBus();
        time = 0;
    }

    private void GetOffBus()
    {
        foreach (var person in people)
        {
            if (person.activeInHierarchy) continue;
//            StartCoroutine(FadeOut(person, 0.0f, 1.0f));
            person.SetActive(true);
            return;
        }
        //ChangeScene();
        SceneActivation();
    }

    private void ChangeScene()
    {
        GetComponent<ChangeScene>().Change();
    }

    private void SceneActivation()
    {
        taActivateScene.PointerEnter();
    }

    /*private IEnumerator FadeOut(GameObject go, float aValue, float aTime)
    {
        Renderer renderer = go.GetComponent<Renderer>();
        Color color = renderer.material.color;
        float timer = 1f;
        
        for (float alpha = 1.0f; alpha > 0f; alpha -= 0.05f)
        {
            Debug.Log(alpha);
            go.GetComponent<Renderer>().material.color = new Color(color.r, color.g, color.b, alpha);
            yield return null;
        }
        
        go.SetActive(false);
    }*/
}
