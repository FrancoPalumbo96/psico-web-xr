﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

public class CheckPlatformTest : MonoBehaviour
{
    [DllImport("__Internal")]
    private static extern bool IsMobile();

    public Text text;
   
    
    // Start is called before the first frame update
    void Start()
    {

        //text.text = isMobile() ? "ESTOY EN WEB MOBIL" : "ESTOY EN WEB PC";
        text.text = "ESTOY EN " + Application.platform + " PLATFORM";
    }
    
    //original
    /*public bool isMobile()
    {
#if !UNITY_EDITOR && UNITY_WEBGL
             return IsMobile();
#endif
        return false;
    }*/

    private bool isMobile()
    {
        /*Debug.Log(IsMobile());
        return IsMobile();*/
        return Application.isMobilePlatform;

    }
}
